---
title: "Exploring KEGG pathways"
output:
  html_document:
    toc: yes
    toc_float: yes
    number_sections: yes
    keep_md: true
    df_print: paged
author: "Laura Carroll, Nicolai Karcher, Alessio Milanese, Jakob Wirbel and
  Georg Zeller"
---

In this practical, we are going to explore functional metagenomic profiles.

```{r setup, include=FALSE}
knitr::opts_chunk$set(collapse=TRUE)
```


# Prepare our R environment

```{r prep, message=FALSE}
library("tidyverse")
library("matrixStats")
library("rjson")
library("pheatmap")
```

# Load the data

First, we load the KEGG functional profiles into a matrix in R:

```{r load_data}
fn.feat <-
  '../../data/functional_profiles/KEGG_kos_FR-CRC.tsv'
kegg.all <- read.table(fn.feat, sep='\t', stringsAsFactors = FALSE,
                       check.names = FALSE, quote = '', row.names = 1,
                       header = TRUE)
```

# Exercise: Data exploration

Similar to the data exploration that we have seen before, get a feeling for
the KEGG matrix: How big is it? What is the distribution of prevalence or
abundance? How would you filter the matrix?

# Filtering

We can perform a total sum scaling again to get the relative abundances of
the individual KEGG KOs.

```{r column_sums}
hist(colSums(kegg.all), 100, col='slategrey', xlab='Sum of columns', main='')
kegg.rel <- prop.table(as.matrix(kegg.all), 2)
```

Now, we can filter the matrix to include only highly abundant modules
- similar to the filtering in the case of taxonomic profiles.

```{r filtering}
max.ab <- rowMaxs(kegg.rel)
hist(log10(max.ab), 100, main='', xlab='Maximum relative abundance',
     col='slategrey')
```

A good cutoff could be `1e-05`.

```{r filtering_2}
cutoff <- 1e-05
kegg.rel.filt <- kegg.rel[which(max.ab > cutoff),]
```

How big is our matrix of functional profiles now after filtering?

```{r size_2}
dim(kegg.rel.filt)
```

In contrast to the taxonomic profiles, we still have several thousand KEGG
kos in our matrix after filtering (compared to several hundreds of mOTUs).
This is a bit problematic for downstream analysis and visualizations.

# KEGG module heatmaps

Since there are too many KEGG KOs to visualize them all together, it makes
sense to focus on specific module or pathways. KEGG modules are collections
of KEGG KOs that jointly perform one or several steps in a metabolic pathway.

## Module definitions

A module definition could look something like this, for example for a 
biotin biosynthesis module:

![](./png/M00577.png)


KEGG KOs which are on the same height form one block. For blocks with several
KEGG KOs, the different KOs can be used interchangeably.

## Load module definitions

The KEGG module definitions are stored in a JSON file:

```{r module_definition}
module.definition <- fromJSON(file='./module_definitions.json')[[1]]$definition
```

For each module, the definition is stored as a string containing several
blocks. Our example module from above would be encoded as:

```{r example_module}
module.definition$M00577
```


## Heatmaps

Now, let us have a look at our biotin biosynthesis module in our dataset. Is 
the complete module covered in our filtered data?

```{r coverage}
definition <- module.definition$M00577
blocks <- str_split(definition, ' ')[[1]]

found.blocks <- 0
for (i in blocks){
  kos <- str_extract_all(i, 'K[0-9]{5}')[[1]]
  if (any(kos %in% rownames(kegg.rel.filt))){
    found.blocks <- found.blocks + 1
  }
}
print(found.blocks/length(blocks))
```

All individual blocks of the module are present in our data. Now, we can
try to visualize the distribution of this module as a heatmap.

```{r}
kos <- str_extract_all(definition, 'K[0-9]{5}')[[1]]
x <- kegg.rel.filt[intersect(kos, rownames(kegg.rel.filt)),]
pheatmap(log10(x+1e-08), cluster_rows = FALSE, show_colnames = FALSE)
```

The coverage does not to be the same across the pathway, since the first KO
is way less abundant than the other KOs (and consistently across samples).


# Further exercises
* What could be the reason for this observation?
* Can you find other biotin biosynthesis modules? 
(Hint: look at [iPATH3](https://pathways.embl.de/)) How do the heatmaps look 
like for those?
* Check out the definition of the module `M00023`. What is the significance
of the `+` symbol in the definition?
* Check out another module of your choice and create a heatmap
using the example dataset.

## iPATH3 and FuncTree2

There are some great resources online to visualize KEGG KOs. Go and check
out [iPATH3](https://pathways.embl.de/) and
[FuncTree2](https://bioviz.tokyo/functree2/).

* Can you find the example modules in the iPATH3 map?
* What kind of data would you need to upload for FuncTree2? Check out the
example on the website and explore the visualization options.
