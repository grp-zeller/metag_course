# Functional metagenome profiling

6. **lecture**: functional metagenomic profiling

7. **demonstration**: mapping to gene catalogues with ngless  

8. **practical**: explore KEGG KOs and modules  

9. **practical**: build HMM for toxins
