---
title: "Demonstration: Functional Profiling with NGLess"
output:
  html_document:
    toc: yes
    toc_float: yes
    number_sections: yes
    keep_md: true
author: "Laura Carroll, Nicolai Karcher, Alessio Milanese, Jakob Wirbel and 
  Georg Zeller"
---

In this demonstration, we are going to explore how `NGLess` can be used for
functional profiling of metagenomic data.

# What is NGLess?

`NGLess` is a domain-specific language which was designed specifically for 
processing of next-generation sequence (NGS) data.

The basic functionality of `NGLess` includes:

* preprocessing and quality control of FastQ files
* mapping to a reference genome (implemented through bwa by default)
* assembly of contigs
* annotation and summarization of the alignments using reference 
gene annotations

It is aimed at everyone who wants to process their sequencing data on their 
own, without necessarily being a method developer.

`NGLess` is not intended to be run on your local machine, since it requires
a lot of memory and processing power to process a typical metagenomic dataset. 
Instead, you will most probably run `NGLess` on a HPC cluster. Therefore, we 
will also not perform a practical here (since it would take to long), but
instead have a small demonstration how `NGLess` is to be used.

For more information, you can also check out the 
[`NGLess` website](https://ngless.embl.de) or the 
[`NGLess` publication](https://microbiomejournal.biomedcentral.com/articles/10.1186/s40168-019-0684-8).

>NG-meta-profiler: fast processing of metagenomes using NGLess, a 
domain-specific language by Luis Pedro Coelho, Renato Alves, Paulo Monteiro, 
Jaime Huerta-Cepas, Ana Teresa Freitas, Peer Bork - Microbiome 2019 7:84; 
https://doi.org/10.1186/s40168-019-0684-8


## Installation

The recommended way to install `NGLess` is via bioconda:

```bash
conda install -c bioconda ngless
```

At the moment `NGLess` only works on Linux machines, but not on Windows or Mac. 
However, it is intended to be used on a HPC cluster or a dedicated data
processing machine, not on your personal laptop.

# Functional Profiling with `NGLess`
## Example script

An example `NGLess` script for functional profiling could look something like
this. In the following section, will explore what each step in this script 
does.

```bash
ngless "1.0"
import "parallel" version "0.6"
import "mocat" version "0.0"
import "igc" version "0.0"

# load samples
samples = readlines('toy_samples')
sample = lock1(samples)
input = load_mocat_sample(sample)

# preprocess reads
input = preprocess(input, keep_singles=True) using |read|:
    read = substrim(read, min_quality=25)
    if len(read) < 45:
        discard

# remove human reads
mapped = map(input, reference='hg38.p10')

mapped = select(mapped) using |mr|:
    mr = mr.filter(min_match_size=45, min_identity_pc=90, action={unmatch})
    if mr.flag({mapped}):
        discard

input = as_reads(mapped)

# map against the IGC
mapped = map(input, reference='igc')


# collect results on gene level
count_igc = count(mapped, multiple={1overN}, 
                  normalization={scaled}, features=['seqname'])
collect(count_igc, current=sample, 
        allneeded=samples, 
        ofile='/output/all_samples.IGC.tsv')

# collect results on KEGG_ko level
count_KEGG = count(mapped, multiple={1overN}, 
                   features=['KEGG_ko'], normalization={scaled})
collect(count_KEGG, current=sample, 
        allneeded=samples, 
        ofile='/output/all_samples.KEGG_ko.tsv')
```


## Module import

At the top of the script, you will have to define which modules you need for
the computation. We want to profile our samples against the IGC, so we
import the `igc` module. 

When the script is run for the first time, `NGLess` will check if the data of 
the IGC have been downloaded already. If not, it will first download the IGC 
and store it somewhere, so that all future calls will be able to access the 
data of the IGC directly. Therefore, the first job can take a bit longer, since
`NGLess` will first have to download 10GB of data.

The other two modules are useful for data handling and parallel processing;
`mocat` is a library for processing of metagenomic data (see for 
[the MOCAT2 website](https://mocat.embl.de/) for more information) and the 
`parallel` module is helpful if you have many samples in the same project.

```bash
ngless "1.0"
import "parallel" version "0.6"
import "mocat" version "0.0"
import "igc" version "0.0"
```


## Load samples via MOCAT

Now, we load a single sample using the `mocat` module. The file `toy_samples`
contains the list of samples in our toy example project. 

```bash
samples = readlines('toy_samples')
sample = lock1(samples)
input = load_mocat_sample(sample)
```

## Preprocessing

As we have seen before, it makes sense to preprocess the reads. Although 
Trimmomatic is not (_yet_) available in `NGLess`, we have several options how
to quality control reads:

* [substrim](https://ngless.embl.de/Functions.html#substrim)
* [endstrim](https://ngless.embl.de/Functions.html#endstrim)
* [smoothtrim](https://ngless.embl.de/Functions.html#smoothtrim)


```bash
input = preprocess(input, keep_singles=True) using |read|:
    read = substrim(read, min_quality=25)
    if len(read) < 45:
        discard
```

## Remove human reads

Since our samples have been collected from human subjects, we most probably
have humans reads in our samples as well. Therefore, we should remove those
before we carry on with our functional profiling. We can do that by mapping
all reads against the human reference genome and then discarding all that
match very well.

```bash
mapped = map(input, reference='hg38.p10')

mapped = select(mapped) using |mr|:
    mr = mr.filter(min_match_size=45, min_identity_pc=90, action={unmatch})
    if mr.flag({mapped}):
        discard

input = as_reads(mapped)
```

`NGLess` has a 
[list of reference genomes](https://ngless.embl.de/Organisms.html) 
available, if you are working with non-human samples. Similarly to the IGC, 
`NGLess` will download the reference genome if it is not yet stored on your 
system.

If you are working with mouse data, for example, you would want to modify this
line in the script with
```bash
mapped = map(input, reference='mm10.p5')
```


## Map against the IGC

Using the `map` function again, we can now map all remaining reads against the 
IGC.

```bash
mapped = map(input, reference='igc')
```

## Collect results

In the last step, we collect the results using the `count` function in 
`NGLess`. Have a look at the 
[documentation](https://ngless.embl.de/Functions.html#count)
to understand what options you have for this function.

For example, we can aggregate the counts on different levels. If we want to 
know the abundances for each individual gene in the IGC, we would do it like
that:

```bash
count_igc = count(mapped, multiple={1overN}, 
                  normalization={scaled}, features=['seqname'])
collect(count_igc, current=sample, 
        allneeded=samples, 
        ofile='/output/all_samples.IGC.tsv')
```

Alternatively, we can also get KEGG_ko abundances by including these lines
in the script.

```bash
count_KEGG = count(mapped, multiple={1overN}, 
                   features=['KEGG_ko'], normalization={scaled})
collect(count_KEGG, current=sample, 
        allneeded=samples, 
        ofile='/output/all_samples.KEGG_ko.tsv')
```


# Actually running `NGLess`

In order to actually run this `NGLess` script, let us look at some toy examples.

## Preparations

In a personal folder on the EMBL cluster, I saved some metagenome samples
from the [Zeller et al.](https://www.ncbi.nlm.nih.gov/pubmed/25432777) 
dataset:

![](./pictures/pic_1.png)


The file `ngless_pipeline.ngl` contains the example script that we have seen
in the section above. The output folder is present, since we want to save our 
results in this folder (see in the script).

The file `toy_samples` contains a list of metagenomes we want to analyse:

![](./pictures/pic_3.png)


The sample folders contain the actual sequencing reads:

![](./pictures/pic_2.png)


I have create my own conda environment for `NGLess`, so I will first activate
this:

```bash
conda activate ngless
```


## Run `NGless` for one sample

Now, I can run the `NGless` script for one sample with the following command:
```bash
ngless ngless_pipeline.ngl
```
As soon as `NGLess` launches, it will first check if your script can actually
be executed or if there are any bugs in there. Furthermore, it will check 
which internal or external modules you will use in your script and will tell 
you which references should be cited:

![](./pictures/pic_4.png)


During the run, `NGLess` will print some basic information about the analysis
on the screen:

![](./pictures/pic_5_1.png)
![](./pictures/pic_5_2.png)


Now, the `NGLess` run has finished. How does our example folder look like now?

![](./pictures/pic_6.png)


As you can see, `NGLess` has create a few folders in which it saves some 
temporary results. Those are:

* `ngless-locks` 
* `ngless-partials` 
* `ngless-stats` 

The locks contain information about which samples have already been processed
by `NGLess`. Samples can either be locked, finished, or failed.

![](./pictures/pic_7.png)


The partials folder contains the partial results. In our case, there is one
folder for the IGC mappings...

![](./pictures/pic_8.png)
... and another folder for KEGG profiles.
![](./pictures/pic_9.png)


Note that each ngless script has its individual hash. Therefore, if you would 
modify the script and run it on the same samples again, the partial results, 
the locks, and the run statistics would be saved in a different subfolder.

The output folder is still empty, because we ran only a single sample so far.

![](./pictures/pic_10.png)

## How to get results?

`NGLess` knows that it has been run on one sample only so far. Therefore, it 
will not save the partial results in the output folder. In order to do so,
we must run `NGLess` for each sample specified in the `toy_samples` file. 
After we have done so, we should see one finished lock for each sample: 

![](./pictures/pic_11.png)


and the results in the `output` folder:

![](./pictures/pic_12.png)
