# metaG_course

Materials for the metagenomic bioinformatic course Feb 2020 (CRC1371 )

# Schedule

## Day 1 (2020-02-18)

| Time  | Lesson  | Title  |
|---|---|---|
| 9:00 - 9:30  | lecture  | Taxonomic profiling  |
| 9:30 - 9:45  | practical  | Quality control and read trimming  |
| 9:45 - 10:00  | practical  | Taxonomic profiling with mOTUs  |
coffee break
| 10:30 - 10:45  | lecture  | Visual exploration of metagenomic data  |
| 10:45 - 12:00  | practical  | Exploratory data analysis  |
lunch
| 13:00 - 14:00  | lecture  | Functional profiling  |
| 14:00 - 14:30  | demonstration  | Functional profiling with NGLess  |
coffee break
| 15:00 - 15:30  | practical  | Explore KEGG modules  |
| 15:30 - 16:30  | practical  | Profile Hidden Markov Models  |
| 16:30 - 17:30  |   | time for discussion / questions  |


## Day 2 (2020-02-19)

| Time  | Lesson  | Title  |
|---|---|---|
| 9:00 - 9:30  | lecture  | Univariate statistics  |
| 9:30 - 12:00 (including coffee break)  | practical  | Association testing and visualization  |
lunch
| 13:00 - 14:15  | lecture  | Machine learning  |
| 14:15 - 16:00 (including coffee break) | practical  | Machine learning  |
| 16:00 - 17:00  |   | Discussion and wrap-up  |

# Preparations 

## Course data

In order to get the data that we will analyze during the course, 
you can download them via a bash terminal (see below)
```bash
wget https://www.embl.de/download/zeller/metaG_course/course_material.zip
unzip course_material.zip
```
or by just clicking 
[this link in your browser](https://www.embl.de/download/zeller/metaG_course/course_material.zip)

## Software requirment

1. You will need a bash terminal. If you are using Windows, you can install [cygwin](https://www.cygwin.com/), if you have Linux or MacOs it will be pre-installed (just open the Terminal).
2. You need to install conda. If you don't have it already you can type within the terminal:    
     For windows: Install miniconda ([link](https://docs.conda.io/en/latest/miniconda.html)) and add it to your path (in cygwin).  
     For MacOS/Linux:  
     `wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`    
     `chmod +x Miniconda3-latest-Linux-x86_64.sh `    
     `./Miniconda3-latest-Linux-x86_64.sh`    
     Say `yes` to install and provide a path to a directory where to install conda.
     Add `conda/bin` to the path.

3. Install the following packages with conda:
    - trimmomatic (`conda install -c bioconda trimmomatic`)
    - fastqc (`conda install -c bioconda fastqc`)
    - mOTUs2 (`conda install -c bioconda motus`)
    - HMMER3 (`conda install -c bioconda hmmer`)
    - muscle (`conda install -c bioconda muscle`)
    - jalview (`conda install -c bioconda jalview`)

4. Please further make sure you have R and Rstudio installed. 
5. Please install the following R packages by running the following commands from within Rstudio:
```R
install.packages("devtools")
devtools::install_github("zellerlab/siamcat")
install.packages("ape")
install.packages("labdsv")
install.packages("tidyverse")
install.packages("vegan")
install.packages("rjson")
install.packages("matrixStats")
install.packages("pheatmap")
```

# Contact

Please feel free to contact us if you have any other questions:

* alessio.milanese[at]embl.de
* laura.carroll[at]embl.de
* jakob.wirbel[at]embl.de
* nicolai.karcher[at]embl.de
* georg.zeller[at]embl.de

