---
title: "Machine Learning for Metagenomic Data"
output:
  html_document:
    toc: yes
    toc_float: yes
    number_sections: yes
    keep_md: true
author: "Laura Carroll, Nicolai Karcher, Alessio Milanese, Jakob Wirbel and 
  Georg Zeller"
---

In this practical, we are going to explore machine learning workflows and
how they can be applied to metagenomic data.



# Preparation of the R environment

First, we prepare our `R` environment:


```r
# load packages
library("tidyverse")
library("SIAMCAT")
library("vegan")

# set a seed to make steps depending on random numbers reproducible
set.seed(2020)
```


# Loading the Data

## Features

First, we are again loading the example dataset as a matrix:


```r
fn.feat.fr  <-
    '../../data/motus_profiles/FR-CRC.motus'
tax.profiles <- read.table(fn.feat.fr, sep = '\t', quote = '', 
                           comment.char = '', skip = 61, 
                           stringsAsFactors = FALSE, check.names = FALSE, 
                           row.names = 1, header = TRUE)
tax.profiles <- as.matrix(tax.profiles)
rel.tax.profiles <- prop.table(tax.profiles, 2)
```

## Metadata

Additionally, we also need the information which sample belongs to which group.
Therefore, we are loading the metadata table as well:


```r
fn.meta.fr  <-
    '../../data/metadata/meta_FR-CRC.tsv'
df.meta <- read_tsv(fn.meta.fr) %>% 
  filter(Group %in% c('CRC', 'CTR')) %>% 
  select(Age, Gender, BMI, AJCC_stage, TNM_stage, 
         Localization, FOBT, Group, Sample_ID) %>% 
  as.data.frame()
rownames(df.meta) <- df.meta$Sample_ID
```


# Machine learning with SIAMCAT


Now, we can create a `SIAMCAT` object, which stores the feature matrix, the 
metadata, and information about the groups you want to compare.


```r
sc.obj <- siamcat(feat=rel.tax.profiles, 
                  meta=df.meta, 
                  label='Group', 
                  case='CRC')
## + starting create.label
## Label used as case:
##    CRC
## Label used as control:
##    CTR
## + finished create.label.from.metadata in 0.023 s
## + starting validate.data
## +++ checking overlap between labels and features
## + Keeping labels of 114 sample(s).
## +++ checking sample number per class
## +++ checking overlap between samples and metadata
## + finished validate.data in 0.326 s
```

## Machine learning workflow

The `SIAMCAT` machine learning workflow consists of several steps:

<!--html_preserve--><div id="htmlwidget-9beb5615d74010a3a9c5" style="width:672px;height:480px;" class="grViz html-widget"></div>
<script type="application/json" data-for="htmlwidget-9beb5615d74010a3a9c5">{"x":{"diagram":"\ndigraph siamcat_workflow {\n\n  # a \"graph\" statement\n  graph [overlap = true, fontsize = 10]\n\n  # several \"node\" statements\n  node [shape = box,\n  fontname = Helvetica,\n  label=\"filter.features\"]\n  C\n\n  node [shape = box,\n  fontname = Helvetica,\n  label=\"normalize.features\"]\n  F\n\n  node [shape = box,\n  fontname = Helvetica,\n  label=\"create.data.split\"]\n  G\n\n  node [shape = box,\n  fontname = Helvetica,\n  label=\"train.model\"]\n  H\n\n  node [shape = box,\n  fontname = Helvetica,\n  label=\"make.predictions\"]\n  I\n\n  node [shape = box,\n  fontname = Helvetica,\n  label=\"evaluate.prediction\"]\n  J\n\n  node [shape = box,\n  fontname = Helvetica,\n  label=\"model.evaluation.plot\"]\n  K\n\n  node [shape = box,\n  fontname = Helvetica,\n  label=\"model.interpretation.plot\"]\n  L\n\n  # several \"edge\" statements\n  C->F\n  F->G G->H H->I I->J J->K\n  J->L\n  }\n","config":{"engine":"dot","options":null}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

## Filtering

As we have seen previously already, we can also use `SIAMCAT` to filter the
feature matrix.


```r
sc.obj <- filter.features(sc.obj, filter.method = 'abundance', cutoff = 0.001)
## Features successfully filtered
sc.obj <- filter.features(sc.obj, filter.method = 'prevalence', cutoff = 0.05,
                          feature.type = 'filtered')
## Features successfully filtered
sc.obj
## siamcat-class object
## label()                Label object:         61 CTR and 53 CRC samples
## filt_feat()            Filtered features:    839 features after abundance, prevalence filtering
## 
## contains phyloseq-class experiment-level object @phyloseq:
## phyloseq@otu_table()   OTU Table:            [ 14213 taxa and 114 samples ]
## phyloseq@sam_data()    Sample Data:          [ 114 samples by 8 sample variables ]
```

## Normalization

`SIAMCAT` offers a few normalization approaches that can be useful for
subsequent statistical modeling in the sense that they transform features in
a way that can increase the accuracy of the resulting models. Importantly,
these normalization techniques do not make use of any label information
(patient status), and can thus be applied up front to the whole data set 
(and outside of the following cross validation).


```r
sc.obj <- normalize.features(sc.obj, norm.method = 'log.std',
                             norm.param = list(log.n0=1e-05, sd.min.q=0))
## Features normalized successfully.
sc.obj
## siamcat-class object
## label()                Label object:         61 CTR and 53 CRC samples
## filt_feat()            Filtered features:    839 features after abundance, prevalence filtering
## norm_feat()            Normalized features:  839 features normalized using log.std
## 
## contains phyloseq-class experiment-level object @phyloseq:
## phyloseq@otu_table()   OTU Table:            [ 14213 taxa and 114 samples ]
## phyloseq@sam_data()    Sample Data:          [ 114 samples by 8 sample variables ]
```

## Model Training

Here we use ten-fold cross validation without resampling and train 
[LASSO logistic regression classifiers](https://www.jstor.org/stable/2346178).


```r
sc.obj <- create.data.split(sc.obj, num.folds = 10, num.resample = 10)
## Features splitted for cross-validation successfully.
sc.obj <- train.model(sc.obj, method='lasso')
## Trained lasso models successfully.
```

## Predictions

This function will automatically apply the models trained in cross validation 
to their respective test sets and aggregate the predictions across the whole 
data set.


```r
sc.obj <- make.predictions(sc.obj)
## Made predictions successfully.
```

Calling the `evaluate.predictions` funtion will result in an assessment of
precision and recall as well as in ROC analysis, both of which can be plotted
as a pdf file using the `model.evaluation.plot` funtion (the name of/path to
the pdf file is passed as an argument).


```r
sc.obj <- evaluate.predictions(sc.obj)
## Evaluated predictions successfully.
model.evaluation.plot(sc.obj, fn.plot = './eval_plot.pdf')
## Plotted evaluation of predictions successfully to: ./eval_plot.pdf
```

![](./eval_plot.png)


Finally, the `model.interpretation.plot` function will plot characteristics 
of the models (i.e. model coefficients or feature importance) alongside the 
input data aiding in understanding how / why the model works (or not).


```r
model.interpretation.plot(sc.obj, consens.thres = 0.7,
                          fn.plot = './interpretation_plot.pdf')
## Successfully plotted model interpretation plot to: ./interpretation_plot.pdf
```

![](./interpretation_plot.png)

# Exercise: Prediction on external data

* In the data folder of the course, you can find another dataset of a 
colorectal cancer metagenomic study. Apply the trained model on this dataset 
and check the model performance on the external dataset.

* Train a `SIAMCAT` model on the Austrian dataset and apply it to the French 
dataset. How does the model transfer on the external dataset compare between
the two datasets? Compare also the feature weights when training on the French
or Austrian dataset.

# Exercise: Taxonomic vs functional predictors

Use `SIAMCAT` to train a model based on the functional KEGG profiles and 
compare it to the one trained on taxonomic profiles.

> Note 1: Since the functional profiles will have many thousands features,
it makes sense to perform feature selection on your dataset. You can 
supply the parameters for the feature selection to the `train.model` function
in `SIAMCAT`.

> Note 2: You can supply several SIAMCAT objects to the function 
`model.evaluation.plot` and compare two ROC curves in the same plot.


# (Optional) Explore machine learning pitfalls

## Loading the data

First, we load the taxonomic profiles again:

```r
fn.feat.kz  <-
  '../../data/motus_profiles/Kushugolova.motus'
feat <- read.table(fn.feat.kz, sep = '\t', quote = '', 
                   comment.char = '', skip = 61, 
                   stringsAsFactors = FALSE, check.names = FALSE, 
                   row.names = 1, header = TRUE)
feat.rel <- prop.table(as.matrix(feat), 2)
```

and the metadata:


```r
fn.meta.kz <- 
  '../../data/metadata/meta_Kushugolova.tsv'
df.meta <- read_tsv(fn.meta.kz) %>% 
  filter(Intervention=='placebo')
## Parsed with column specification:
## cols(
##   Sample_ID = col_character(),
##   Individual_ID = col_character(),
##   Timepoint = col_double(),
##   Gender = col_character(),
##   Age = col_double(),
##   BMI = col_double(),
##   Country = col_character(),
##   Group = col_character(),
##   Intervention = col_character(),
##   Western = col_character()
## )
```

## Prepare `SIAMCAT` object

We can create a `SIAMCAT` object for the data and perform the basic steps of
preprocessing.


```r
meta <- as.data.frame(df.meta)
rownames(meta) <- meta$Sample_ID

sc.obj <- siamcat(feat=feat.rel, meta=meta, label='Group', case='MS')
## + starting create.label
## Label used as case:
##    MS
## Label used as control:
##    CTR
## + finished create.label.from.metadata in 0.002 s
## + starting validate.data
## +++ checking overlap between labels and features
## + Keeping labels of 84 sample(s).
## +++ checking sample number per class
## +++ checking overlap between samples and metadata
## + finished validate.data in 0.348 s
sc.obj <- filter.features(sc.obj, filter.method = 'abundance', cutoff = 1e-03)
## Features successfully filtered
sc.obj <- filter.features(sc.obj, filter.method = 'prevalence', cutoff = 0.05,
                          feature.type = 'filtered')
## Features successfully filtered
sc.obj <- normalize.features(sc.obj, norm.method = 'log.std', 
                             norm.param = list(log.n0=1e-05, sd.min.q=0))
## Features normalized successfully.
```

## Similarity across and within individuals

In this dataset, we have repeated samplings for the same individual:

```r
nrow(meta)
## [1] 84
length(unique(meta$Individual_ID))
## [1] 42
```

This means that the sample are not independent from each other. Samples from
the same individual are ususally much more similar towards each other than 
across individuals:


```r
feat.filt <- get.filt_feat.matrix(sc.obj)
feat.log <- log10(feat.filt + 1e-05)

log.euc.dist <- as.matrix(dist(t(feat.log)))
diag(log.euc.dist) <- NA
log.euc.dist[lower.tri(log.euc.dist)] <- NA

df.plot <- as_tibble(log.euc.dist) %>% 
  mutate(Sample=rownames(log.euc.dist)) %>% 
  pivot_longer(-Sample) %>% 
  filter(!is.na(value))
df.plot <- left_join(df.plot, meta %>%
                       transmute(Sample=Sample_ID, ID1=Individual_ID)) %>% 
  left_join(meta %>% transmute(name=Sample_ID, ID2=Individual_ID)) %>% 
  mutate(type=ID1==ID2)
## Joining, by = "Sample"
## Joining, by = "name"

df.plot %>% 
  ggplot(aes(x=type, y=value, fill=type)) + 
    geom_boxplot() + 
    xlab('Same individual?') + 
    ylab('log-Euclidean distance')
```

![](machine_learning_files/figure-html/similarity-1.png)<!-- -->


## Naive model training

Without taking this fact into account, we can train a naive model quickly:


```r
sc.obj.1 <- create.data.split(sc.obj, num.folds = 10, num.resample = 10)
## Features splitted for cross-validation successfully.
sc.obj.1 <- train.model(sc.obj.1, method='lasso')
## Trained lasso models successfully.
sc.obj.1 <- make.predictions(sc.obj.1)
## Made predictions successfully.
sc.obj.1 <- evaluate.predictions(sc.obj.1)
## Evaluated predictions successfully.
```


## Adjusted model training

In order to adjust the pipeline so that the cross validation split will take
into account, that some samples come from the same individual, we need to make
only one small modification:


```r
sc.obj.2 <- create.data.split(sc.obj, num.folds = 10, num.resample = 10,
                              inseparable = 'Individual_ID')
## Features splitted for cross-validation successfully.
sc.obj.2 <- train.model(sc.obj.2, method='lasso')
## Trained lasso models successfully.
sc.obj.2 <- make.predictions(sc.obj.2)
## Made predictions successfully.
sc.obj.2 <- evaluate.predictions(sc.obj.2)
## Evaluated predictions successfully.
```


## Compare

Finally, we can compare the performance of the two different approaches:


```r
model.evaluation.plot(sc.obj.1, sc.obj.2, fn.plot = './kushugolova_eval.pdf')
## Plotted evaluation of predictions successfully to: ./kushugolova_eval.pdf
```

![](./kushugolova_eval.png)

In our example, the naive model (blue) seems to perform way better than the 
adjusted model (red). However, since the samples are not independent of each 
other, the naive procedure dramatically overestimates the true performance.
Therefore, this model finds a strong signal although -in reality- the 
difference between the two groups is not that striking.
