---
title: "Association Testing for Metagenomic Data"
output:
  html_document:
    toc: yes
    toc_float: yes
    number_sections: yes
    keep_md: true
    df_print: paged
author: "Laura Carroll, Nicolai Karcher, Alessio Milanese, Jakob Wirbel and 
  Georg Zeller"
---

In this practical, we are going to explore statistical testing methods for 
metagenomic data and how to visualize the results.




# Preparation of the R environment

First, we prepare our `R` environment:


```r
# load packages
library("tidyverse")
library("SIAMCAT")
library("vegan")
library("labdsv")
library("ape")
# set a seed to make steps depending on random numbers reproducible
set.seed(2020)
```

# Loading the Data

## Features

First, we are again loading the example dataset as a matrix:


```r
fn.feat.fr  <-
    '../../data/motus_profiles/FR-CRC.motus'
tax.profiles <- read.table(fn.feat.fr, sep = '\t', quote = '', 
                           comment.char = '', skip = 61, 
                           stringsAsFactors = FALSE, check.names = FALSE, 
                           row.names = 1, header = TRUE)
tax.profiles <- as.matrix(tax.profiles)
rel.tax.profiles <- prop.table(tax.profiles, 2)
```

## Metadata

Additionally, we also need the information which sample belongs to which group.
Therefore, we are loading the metadata table as well:


```r
fn.meta.fr  <-
    '../../data/metadata/meta_FR-CRC.tsv'
df.meta <- read_tsv(fn.meta.fr)
df.meta
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":["Sample_ID"],"name":[1],"type":["chr"],"align":["left"]},{"label":["External_ID"],"name":[2],"type":["chr"],"align":["left"]},{"label":["Age"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["Gender"],"name":[4],"type":["chr"],"align":["left"]},{"label":["BMI"],"name":[5],"type":["dbl"],"align":["right"]},{"label":["Country"],"name":[6],"type":["chr"],"align":["left"]},{"label":["AJCC_stage"],"name":[7],"type":["chr"],"align":["left"]},{"label":["TNM_stage"],"name":[8],"type":["chr"],"align":["left"]},{"label":["Localization"],"name":[9],"type":["chr"],"align":["left"]},{"label":["Study"],"name":[10],"type":["chr"],"align":["left"]},{"label":["Sampling_rel_to_colonoscopy"],"name":[11],"type":["chr"],"align":["left"]},{"label":["FOBT"],"name":[12],"type":["chr"],"align":["left"]},{"label":["Group"],"name":[13],"type":["chr"],"align":["left"]},{"label":["Diabetes"],"name":[14],"type":["lgl"],"align":["right"]},{"label":["Vegetarian"],"name":[15],"type":["lgl"],"align":["right"]},{"label":["Smoking"],"name":[16],"type":["lgl"],"align":["right"]},{"label":["Library_Size"],"name":[17],"type":["dbl"],"align":["right"]}],"data":[{"1":"CCIS00146684ST-4-0","2":"FR-726","3":"72","4":"F","5":"25","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"35443944"},{"1":"CCIS00281083ST-3-0","2":"FR-060","3":"53","4":"M","5":"32","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"19307896"},{"1":"CCIS02124300ST-4-0","2":"FR-568","3":"35","4":"M","5":"23","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"42141246"},{"1":"CCIS02379307ST-4-0","2":"FR-828","3":"67","4":"M","5":"28","6":"FRA","7":"I","8":"T1N0M0","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"4829533"},{"1":"CCIS02856720ST-4-0","2":"FR-027","3":"74","4":"M","5":"27","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CTR","14":"NA","15":"NA","16":"NA","17":"34294675"},{"1":"CCIS03473770ST-4-0","2":"FR-192","3":"29","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"20262319"},{"1":"CCIS03857607ST-4-0","2":"FR-349","3":"61","4":"M","5":"23","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"29917559"},{"1":"CCIS05314658ST-4-0","2":"FR-169","3":"65","4":"F","5":"24","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"28453362"},{"1":"CCIS06260551ST-3-0","2":"FR-200","3":"58","4":"M","5":"24","6":"FRA","7":"IV","8":"T3N1M1","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"15598843"},{"1":"CCIS07277498ST-4-0","2":"FR-276","3":"63","4":"M","5":"NA","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"NA","13":"CTR","14":"NA","15":"NA","16":"NA","17":"31510809"},{"1":"CCIS07539127ST-4-0","2":"FR-460","3":"77","4":"F","5":"22","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"32025452"},{"1":"CCIS07648107ST-4-0","2":"FR-053","3":"62","4":"F","5":"21","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"30986245"},{"1":"CCIS08668806ST-3-0","2":"FR-214","3":"63","4":"M","5":"26","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"8467141"},{"1":"CCIS09568613ST-4-0","2":"FR-400","3":"67","4":"M","5":"21","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"35354460"},{"1":"CCIS10706551ST-3-0","2":"FR-193","3":"25","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"28855308"},{"1":"CCIS10793554ST-4-0","2":"FR-606","3":"64","4":"M","5":"26","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"18941421"},{"1":"CCIS11015875ST-4-0","2":"FR-788","3":"62","4":"M","5":"26","6":"FRA","7":"III","8":"T3N1M0","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"26824567"},{"1":"CCIS11019776ST-4-0","2":"FR-548","3":"58","4":"M","5":"22","6":"FRA","7":"NA","8":"NA","9":"LC/RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"22249555"},{"1":"CCIS11354283ST-4-0","2":"FR-212","3":"67","4":"M","5":"27","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"28417199"},{"1":"CCIS11362406ST-4-0","2":"FR-398","3":"44","4":"F","5":"26","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"46574413"},{"1":"CCIS11558985ST-4-0","2":"FR-902","3":"56","4":"F","5":"NA","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"17480615"},{"1":"CCIS12370844ST-4-0","2":"FR-823","3":"81","4":"F","5":"39","6":"FRA","7":"I","8":"T2N0M0","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"1981993"},{"1":"CCIS12656533ST-4-0","2":"FR-654","3":"51","4":"M","5":"30","6":"FRA","7":"IV","8":"T2N1M1","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"21293790"},{"1":"CCIS13047523ST-4-0","2":"FR-003","3":"70","4":"M","5":"22","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"29557894"},{"1":"CCIS14449628ST-4-0","2":"FR-404","3":"59","4":"F","5":"20","6":"FRA","7":"III","8":"T4N1M0","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"22367285"},{"1":"CCIS15704761ST-4-0","2":"FR-901","3":"56","4":"F","5":"NA","6":"FRA","7":"IV","8":"T4N1M1","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"19267185"},{"1":"CCIS15794887ST-4-0","2":"FR-024","3":"37","4":"F","5":"18","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"19330062"},{"1":"CCIS16326685ST-4-0","2":"FR-542","3":"46","4":"F","5":"29","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"24000063"},{"1":"CCIS16383318ST-4-0","2":"FR-139","3":"61","4":"F","5":"24","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"36238309"},{"1":"CCIS16561622ST-4-0","2":"FR-030","3":"54","4":"M","5":"26","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"41280291"},{"1":"CCIS17669415ST-4-0","2":"FR-125","3":"72","4":"F","5":"37","6":"FRA","7":"I","8":"T1N0M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"30538779"},{"1":"CCIS19142497ST-3-0","2":"FR-051","3":"68","4":"M","5":"23","6":"FRA","7":"NA","8":"NA","9":"LC/RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"12933986"},{"1":"CCIS20795251ST-4-0","2":"FR-221","3":"71","4":"F","5":"23","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"25358270"},{"1":"CCIS21126322ST-4-0","2":"FR-121","3":"59","4":"M","5":"25","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"36157541"},{"1":"CCIS21278152ST-4-0","2":"FR-414","3":"63","4":"M","5":"25","6":"FRA","7":"II","8":"T3N0M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"23458416"},{"1":"CCIS22275061ST-4-0","2":"FR-191","3":"62","4":"M","5":"29","6":"FRA","7":"NA","8":"NA","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"42358438"},{"1":"CCIS22416007ST-4-0","2":"FR-815","3":"82","4":"M","5":"24","6":"FRA","7":"IV","8":"T4N1M1","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"42178363"},{"1":"CCIS22906510ST-20-0","2":"FR-826","3":"77","4":"M","5":"23","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"NAA","14":"NA","15":"NA","16":"NA","17":"40587980"},{"1":"CCIS22958137ST-20-0","2":"FR-506","3":"60","4":"M","5":"26","6":"FRA","7":"IV","8":"T4N1M1","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"46226004"},{"1":"CCIS23164343ST-4-0","2":"FR-316","3":"62","4":"M","5":"23","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"32281417"},{"1":"CCIS24254057ST-4-0","2":"FR-767","3":"69","4":"F","5":"24","6":"FRA","7":"IV","8":"T4N1M1","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"61932153"},{"1":"CCIS24898163ST-4-0","2":"FR-282","3":"59","4":"M","5":"31","6":"FRA","7":"NA","8":"NA","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"5647333"},{"1":"CCIS25399172ST-4-0","2":"FR-419","3":"60","4":"M","5":"27","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"13361371"},{"1":"CCIS27304052ST-3-0","2":"FR-001","3":"52","4":"F","5":"20","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"19441026"},{"1":"CCIS27927933ST-4-0","2":"FR-115","3":"72","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"44183270"},{"1":"CCIS28384594ST-4-0","2":"FR-382","3":"89","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"25528379"},{"1":"CCIS29210128ST-4-0","2":"FR-166","3":"64","4":"M","5":"27","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"21601905"},{"1":"CCIS29688262ST-20-0","2":"FR-305","3":"50","4":"M","5":"25","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"38854418"},{"1":"CCIS31434951ST-20-0","2":"FR-643","3":"66","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"13840048"},{"1":"CCIS32105356ST-4-0","2":"FR-510","3":"53","4":"M","5":"30","6":"FRA","7":"NA","8":"NA","9":"LC/RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"23774880"},{"1":"CCIS32452666ST-4-0","2":"FR-105","3":"68","4":"M","5":"26","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"25019747"},{"1":"CCIS33816588ST-4-0","2":"FR-652","3":"64","4":"M","5":"21","6":"FRA","7":"III","8":"T3N1M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"30493004"},{"1":"CCIS34055159ST-4-0","2":"FR-430","3":"80","4":"M","5":"29","6":"FRA","7":"I","8":"T2N0M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"21250090"},{"1":"CCIS34604008ST-4-0","2":"FR-129","3":"63","4":"F","5":"27","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"33724649"},{"1":"CCIS35092938ST-4-0","2":"FR-500","3":"62","4":"F","5":"21","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"20654081"},{"1":"CCIS35100175ST-4-0","2":"FR-812","3":"73","4":"M","5":"40","6":"FRA","7":"I","8":"T1N0M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"25119675"},{"1":"CCIS36699628ST-4-0","2":"FR-142","3":"68","4":"F","5":"25","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"31159348"},{"1":"CCIS36797902ST-4-0","2":"FR-198","3":"63","4":"M","5":"25","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"19681060"},{"1":"CCIS37250421ST-4-0","2":"FR-496","3":"59","4":"F","5":"21","6":"FRA","7":"NA","8":"NA","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"NAA","14":"NA","15":"NA","16":"NA","17":"22577160"},{"1":"CCIS38765456ST-20-0","2":"FR-723","3":"79","4":"F","5":"22","6":"FRA","7":"IV","8":"T4N1M1","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"38274855"},{"1":"CCIS40244499ST-3-0","2":"FR-202","3":"87","4":"F","5":"32","6":"FRA","7":"II","8":"T4N0M0","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"24505848"},{"1":"CCIS41222843ST-4-0","2":"FR-298","3":"73","4":"F","5":"24","6":"FRA","7":"III","8":"T3N1M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"42370435"},{"1":"CCIS41288781ST-4-0","2":"FR-817","3":"74","4":"F","5":"19","6":"FRA","7":"II","8":"T3N0M0","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"20807788"},{"1":"CCIS41548810ST-4-0","2":"FR-451","3":"78","4":"M","5":"19","6":"FRA","7":"IV","8":"T3N0M1","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"25909920"},{"1":"CCIS41692898ST-4-0","2":"FR-558","3":"67","4":"F","5":"28","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"35216845"},{"1":"CCIS41806458ST-4-0","2":"FR-664","3":"59","4":"F","5":"NA","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"NAA","14":"NA","15":"NA","16":"NA","17":"27606591"},{"1":"CCIS44093303ST-4-0","2":"FR-721","3":"62","4":"F","5":"20","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CTR","14":"NA","15":"NA","16":"NA","17":"34539245"},{"1":"CCIS44676181ST-4-0","2":"FR-161","3":"48","4":"F","5":"21","6":"FRA","7":"NA","8":"NA","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"19955843"},{"1":"CCIS44743950ST-4-0","2":"FR-596","3":"66","4":"M","5":"22","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"38044446"},{"1":"CCIS44757994ST-4-0","2":"FR-770","3":"75","4":"M","5":"37","6":"FRA","7":"III","8":"T3N1M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"22702296"},{"1":"CCIS45571137ST-3-0","2":"FR-116","3":"55","4":"F","5":"20","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"15000419"},{"1":"CCIS45793747ST-4-0","2":"FR-684","3":"67","4":"M","5":"25","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"39708809"},{"1":"CCIS46047672ST-4-0","2":"FR-825","3":"65","4":"M","5":"24","6":"FRA","7":"IV","8":"T4N0M1","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"18277769"},{"1":"CCIS46467422ST-4-0","2":"FR-830","3":"54","4":"F","5":"22","6":"FRA","7":"III","8":"T3N1M0","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"5534486"},{"1":"CCIS47284573ST-4-0","2":"FR-667","3":"67","4":"M","5":"25","6":"FRA","7":"III","8":"T4N1M0","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"18352816"},{"1":"CCIS47745468ST-4-0","2":"FR-054","3":"63","4":"M","5":"27","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"36205688"},{"1":"CCIS48174381ST-4-0","2":"FR-751","3":"66","4":"M","5":"25","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"24187553"},{"1":"CCIS48507077ST-4-0","2":"FR-507","3":"72","4":"F","5":"32","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"26359362"},{"1":"CCIS48579360ST-4-0","2":"FR-829","3":"52","4":"F","5":"23","6":"FRA","7":"NA","8":"NA","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"10676683"},{"1":"CCIS48725289ST-4-0","2":"FR-170","3":"62","4":"F","5":"21","6":"FRA","7":"I","8":"T1N0M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"15677334"},{"1":"CCIS50003399ST-4-0","2":"FR-194","3":"66","4":"F","5":"28","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"29725483"},{"1":"CCIS50148151ST-4-0","2":"FR-503","3":"87","4":"F","5":"15","6":"FRA","7":"III","8":"T2N1M0","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"24166049"},{"1":"CCIS50369211ST-20-0","2":"FR-215","3":"53","4":"F","5":"33","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"9842615"},{"1":"CCIS50471204ST-4-0","2":"FR-213","3":"61","4":"F","5":"23","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"38401964"},{"1":"CCIS50561855ST-4-0","2":"FR-474","3":"60","4":"F","5":"26","6":"FRA","7":"NA","8":"NA","9":"LC/RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"14986182"},{"1":"CCIS51595129ST-4-0","2":"FR-399","3":"63","4":"F","5":"22","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"27642810"},{"1":"CCIS51667829ST-4-0","2":"FR-113","3":"63","4":"M","5":"26","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"35840774"},{"1":"CCIS52234160ST-4-0","2":"FR-473","3":"64","4":"M","5":"28","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"22346644"},{"1":"CCIS52370277ST-4-0","2":"FR-827","3":"53","4":"M","5":"25","6":"FRA","7":"IV","8":"T4N1M1","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"26193276"},{"1":"CCIS53043478ST-4-0","2":"FR-551","3":"72","4":"M","5":"22","6":"FRA","7":"IV","8":"T3N1M1","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"17962548"},{"1":"CCIS53355328ST-4-0","2":"FR-293","3":"76","4":"M","5":"NA","6":"FRA","7":"I","8":"T2N0M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"17433179"},{"1":"CCIS53557295ST-4-0","2":"FR-666","3":"45","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"14671389"},{"1":"CCIS54027808ST-4-0","2":"FR-835","3":"52","4":"M","5":"33","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"NAA","14":"NA","15":"NA","16":"NA","17":"22030805"},{"1":"CCIS55230578ST-4-0","2":"FR-734","3":"64","4":"M","5":"30","6":"FRA","7":"IV","8":"T3N1M1","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"27804303"},{"1":"CCIS55531770ST-4-0","2":"FR-790","3":"45","4":"F","5":"24","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"6745142"},{"1":"CCIS56503244ST-3-0","2":"FR-162","3":"68","4":"M","5":"21","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"13595352"},{"1":"CCIS58234805ST-4-0","2":"FR-268","3":"70","4":"F","5":"26","6":"FRA","7":"III","8":"T3N1M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"11012918"},{"1":"CCIS59132091ST-4-0","2":"FR-617","3":"80","4":"F","5":"23","6":"FRA","7":"I","8":"T1N0M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"47653407"},{"1":"CCIS59903910ST-4-0","2":"FR-820","3":"76","4":"F","5":"34","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"24858698"},{"1":"CCIS61287323ST-4-0","2":"FR-376","3":"76","4":"M","5":"25","6":"FRA","7":"II","8":"T3N0M0","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"35663674"},{"1":"CCIS62605362ST-3-0","2":"FR-504","3":"56","4":"F","5":"23","6":"FRA","7":"II","8":"T3N0M0","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"58613057"},{"1":"CCIS62794166ST-4-0","2":"FR-780","3":"68","4":"M","5":"23","6":"FRA","7":"IV","8":"T3N1M1","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"18137582"},{"1":"CCIS63448910ST-4-0","2":"FR-390","3":"61","4":"F","5":"34","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"29010067"},{"1":"CCIS63468405ST-4-0","2":"FR-132","3":"69","4":"F","5":"25","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"31230015"},{"1":"CCIS63910149ST-4-0","2":"FR-719","3":"49","4":"F","5":"23","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"7403037"},{"1":"CCIS64773582ST-4-0","2":"FR-195","3":"63","4":"F","5":"23","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"43574702"},{"1":"CCIS64785924ST-20-0","2":"FR-173","3":"59","4":"F","5":"25","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"5953194"},{"1":"CCIS65479369ST-4-0","2":"FR-772","3":"69","4":"F","5":"25","6":"FRA","7":"IV","8":"T3N1M1","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"7732781"},{"1":"CCIS70398272ST-4-0","2":"FR-792","3":"55","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"36295986"},{"1":"CCIS71301801ST-4-0","2":"FR-281","3":"50","4":"F","5":"24","6":"FRA","7":"I","8":"T1N0M0","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"45032701"},{"1":"CCIS71578391ST-4-0","2":"FR-187","3":"70","4":"M","5":"25","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"34251424"},{"1":"CCIS72607085ST-4-0","2":"FR-824","3":"74","4":"F","5":"26","6":"FRA","7":"IV","8":"T3NxM1","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"21729304"},{"1":"CCIS74239020ST-4-0","2":"FR-156","3":"62","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"28128807"},{"1":"CCIS74726977ST-3-0","2":"FR-026","3":"66","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"LC/RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"62926247"},{"1":"CCIS76563044ST-4-0","2":"FR-672","3":"68","4":"M","5":"36","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"35200771"},{"1":"CCIS76845094ST-20-0","2":"FR-450","3":"44","4":"M","5":"20","6":"FRA","7":"IV","8":"T3N1M1","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"72543335"},{"1":"CCIS77100899ST-4-0","2":"FR-211","3":"73","4":"M","5":"26","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"32162657"},{"1":"CCIS77252613ST-4-0","2":"FR-783","3":"65","4":"M","5":"26","6":"FRA","7":"I","8":"T1N0M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"43032257"},{"1":"CCIS78100604ST-4-0","2":"FR-728","3":"64","4":"F","5":"30","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"36588707"},{"1":"CCIS78318719ST-4-0","2":"FR-768","3":"69","4":"F","5":"30","6":"FRA","7":"II","8":"T3N0M0","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"26554667"},{"1":"CCIS79210440ST-3-0","2":"FR-039","3":"65","4":"M","5":"30","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CTR","14":"NA","15":"NA","16":"NA","17":"17338108"},{"1":"CCIS80834637ST-4-0","2":"FR-716","3":"60","4":"F","5":"28","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"44830160"},{"1":"CCIS81139242ST-4-0","2":"FR-118","3":"64","4":"F","5":"20","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"29841558"},{"1":"CCIS81710917ST-20-0","2":"FR-539","3":"49","4":"M","5":"20","6":"FRA","7":"NA","8":"NA","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"48035727"},{"1":"CCIS81735969ST-20-0","2":"FR-312","3":"68","4":"M","5":"26","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"34764065"},{"1":"CCIS81887263ST-4-0","2":"FR-302","3":"59","4":"M","5":"27","6":"FRA","7":"I","8":"T2N0M0","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"43180950"},{"1":"CCIS82146115ST-4-0","2":"FR-628","3":"51","4":"M","5":"24","6":"FRA","7":"IV","8":"T3N1M1","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"43960694"},{"1":"CCIS82507866ST-3-0","2":"FR-040","3":"57","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"18262334"},{"1":"CCIS82944710ST-20-0","2":"FR-730","3":"38","4":"F","5":"22","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"4215880"},{"1":"CCIS83445808ST-4-0","2":"FR-495","3":"66","4":"M","5":"29","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"19501458"},{"1":"CCIS83574003ST-4-0","2":"FR-241","3":"60","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"ADA","14":"NA","15":"NA","16":"NA","17":"38304657"},{"1":"CCIS83870198ST-4-0","2":"FR-344","3":"48","4":"F","5":"24","6":"FRA","7":"I","8":"T1N0M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"33343498"},{"1":"CCIS84543192ST-4-0","2":"FR-722","3":"45","4":"F","5":"28","6":"FRA","7":"I","8":"T2N0M0","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"14973046"},{"1":"CCIS85214191ST-3-0","2":"FR-208","3":"63","4":"M","5":"22","6":"FRA","7":"IV","8":"T3N1M1","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"26950900"},{"1":"CCIS87116798ST-4-0","2":"FR-310","3":"85","4":"F","5":"20","6":"FRA","7":"IV","8":"T3N1M1","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"22643954"},{"1":"CCIS87167916ST-4-0","2":"FR-223","3":"79","4":"M","5":"30","6":"FRA","7":"IV","8":"T4N1M1","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"20100401"},{"1":"CCIS87252800ST-4-0","2":"FR-505","3":"73","4":"M","5":"17","6":"FRA","7":"I","8":"T2N0M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"11753596"},{"1":"CCIS87290971ST-4-0","2":"FR-196","3":"71","4":"M","5":"29","6":"FRA","7":"NA","8":"NA","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"27257848"},{"1":"CCIS87605453ST-4-0","2":"FR-328","3":"73","4":"F","5":"26","6":"FRA","7":"II","8":"T3N0M0","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"39141969"},{"1":"CCIS88007743ST-4-0","2":"FR-393","3":"67","4":"F","5":"23","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"29101486"},{"1":"CCIS88317640ST-4-0","2":"FR-759","3":"74","4":"M","5":"27","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"33771666"},{"1":"CCIS90164298ST-4-0","2":"FR-294","3":"84","4":"M","5":"23","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"30094526"},{"1":"CCIS90166425ST-4-0","2":"FR-449","3":"78","4":"F","5":"23","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"27271452"},{"1":"CCIS90443472ST-4-0","2":"FR-249","3":"63","4":"M","5":"30","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"31948167"},{"1":"CCIS90903952ST-3-0","2":"FR-218","3":"71","4":"F","5":"22","6":"FRA","7":"NA","8":"NA","9":"LC/RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"ADA","14":"NA","15":"NA","16":"NA","17":"22788163"},{"1":"CCIS91228662ST-4-0","2":"FR-275","3":"63","4":"M","5":"31","6":"FRA","7":"I","8":"T1N0M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"44044852"},{"1":"CCIS93040568ST-20-0","2":"FR-682","3":"65","4":"M","5":"30","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"6436717"},{"1":"CCIS94417875ST-3-0","2":"FR-110","3":"59","4":"F","5":"25","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"18113211"},{"1":"CCIS94496512ST-4-0","2":"FR-229","3":"64","4":"M","5":"28","6":"FRA","7":"NA","8":"NA","9":"Rectum","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"17750885"},{"1":"CCIS94603952ST-4-0","2":"FR-025","3":"80","4":"F","5":"21","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"32451093"},{"1":"CCIS95097901ST-4-0","2":"FR-696","3":"52","4":"M","5":"24","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"24039454"},{"1":"CCIS95409808ST-4-0","2":"FR-152","3":"63","4":"F","5":"21","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"31515564"},{"1":"CCIS96387239ST-4-0","2":"FR-626","3":"66","4":"M","5":"20","6":"FRA","7":"NA","8":"NA","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"NAA","14":"NA","15":"NA","16":"NA","17":"15901300"},{"1":"CCIS98482370ST-3-0","2":"FR-052","3":"53","4":"F","5":"30","6":"FRA","7":"NA","8":"NA","9":"NA","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CTR","14":"NA","15":"NA","16":"NA","17":"18276972"},{"1":"CCIS98512455ST-4-0","2":"FR-459","3":"63","4":"M","5":"29","6":"FRA","7":"IV","8":"T4N1M1","9":"RC","10":"FR-CRC","11":"BEFORE","12":"Negative","13":"CRC","14":"NA","15":"NA","16":"NA","17":"20597661"},{"1":"CCIS98832363ST-4-0","2":"FR-552","3":"55","4":"F","5":"25","6":"FRA","7":"III","8":"T3N1M0","9":"LC","10":"FR-CRC","11":"BEFORE","12":"Positive","13":"CRC","14":"NA","15":"NA","16":"NA","17":"30762491"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

```r
table(df.meta$Group)
## 
## ADA CRC CTR NAA 
##  15  53  61  27
```

We are interested in the comparison between control samples (`CTR`) and 
colorectal cancer samples (`CRC`), so we can first remove the other samples, 
which represent advanced adenoma (`ADA`) or non-advanced adenoma (`NAA`).
Also, we can transform the metadata into a data.frame object (which is easier 
for some analyses later on).


```r
df.meta <- df.meta %>% 
  filter(Group %in% c('CRC', 'CTR')) %>% 
  as.data.frame()
rownames(df.meta) <- df.meta$Sample_ID

tax.profiles <- tax.profiles[,rownames(df.meta)]
rel.tax.profiles <- rel.tax.profiles[,rownames(df.meta)]
```

## Feature filtering

As we have done yesterday, we first filter the taxonomic profiles again based
on the maximum species abundance:


```r
species.max.value <- apply(rel.tax.profiles, 1, max)
f.idx <- which(species.max.value > 1e-03)
rel.tax.profiles.filt <- rel.tax.profiles[f.idx,]
idx.um <- which(rownames(rel.tax.profiles.filt) == '-1')
rel.tax.profiles.filt <- rel.tax.profiles.filt[-idx.um,]
```


# Exercise: Ordination

Yesterday, we explored already how to calculate an ordination. Create an 
ordination plot for our data but colour the samples by disease. 
How would you interpret the results? Try out different ecological distances. 
How does the choice of distance affect the group separation?

# Association Testing

To test for statistically significant differences, we perform a Wilcoxon test
on each individual bacterial species.


```r
p.vals <- rep_len(1, nrow(rel.tax.profiles.filt))
names(p.vals) <- rownames(rel.tax.profiles.filt)
stopifnot(all(rownames(df.meta) == colnames(rel.tax.profiles.filt)))
for (i in rownames(rel.tax.profiles.filt)){
  x <- rel.tax.profiles.filt[i,]
  y <- df.meta$Group
  t <- wilcox.test(x~y)
  p.vals[i] <- t$p.value
}
head(sort(p.vals))
## Fusobacterium nucleatum subsp. animalis [ref_mOTU_v25_01001] 
##                                                 1.072932e-07 
##                  Dialister pneumosintes [ref_mOTU_v25_03630] 
##                                                 5.047528e-06 
##                    Hungatella hathewayi [ref_mOTU_v25_03436] 
##                                                 3.079069e-05 
##           Olsenella sp. Marseille-P2300 [ref_mOTU_v25_10001] 
##                                                 2.283988e-04 
##                         Clostridium sp. [ref_mOTU_v25_03680] 
##                                                 2.756465e-04 
##                   [Eubacterium] eligens [ref_mOTU_v25_02375] 
##                                                 3.395879e-04
```

The species with the strongest differential abundance seems to be
Fusobacterium nucleatum, so let us take a look at the distribution of this 
species:


```r
i <- 'Fusobacterium nucleatum subsp. animalis [ref_mOTU_v25_01001]'
df.plot <- tibble(fuso=rel.tax.profiles.filt[i,],
                  label=df.meta$Group)
df.plot %>% 
  ggplot(aes(x=label, y=fuso)) + 
    geom_boxplot() + 
    xlab('') + 
    ylab('F. nucleatum rel. ab.')
```

![](association_testing_files/figure-html/fuso-1.png)<!-- -->

Let us remember that log-scales are important when visualizing relative 
abundance data!


```r
df.plot %>% 
  ggplot(aes(x=label, y=log10(fuso + 1e-05))) + 
    geom_boxplot(outlier.shape = NA) +
    geom_jitter(width = 0.08) + 
    xlab('') + 
    ylab('log10(F. nucleatum rel. ab.)')
```

![](association_testing_files/figure-html/fuso_2-1.png)<!-- -->

# Exercises: Visualization 

* Explore the significantly associated species using boxplots or other 
visualization techniques. Which is the most strongly associated species that
is enriched in controls?

* Plot a volcano plot of the associations between cancer and controls. Which
effect size could you use for the volcano plot?

* Plot a heatmap of the 20 top associated bacterial species. Add the 
disease status of the different samples as annotation to the heatmap.


# SIAMCAT Association Testing

We can also use the `SIAMCAT` R package to test for differential abundance and
produce standard visualizations.


```r
library("SIAMCAT")
```

Within `SIAMCAT`, the data are stored in the `SIAMCAT` object which contains 
the feature matrix, the metadata, and information about the groups you want to
compare.


```r
sc.obj <- siamcat(feat=rel.tax.profiles, meta=df.meta, 
                  label='Group', case='CRC')
## + starting create.label
## Label used as case:
##    CRC
## Label used as control:
##    CTR
## + finished create.label.from.metadata in 0.024 s
## + starting validate.data
## +++ checking overlap between labels and features
## + Keeping labels of 114 sample(s).
## +++ checking sample number per class
## +++ checking overlap between samples and metadata
## + finished validate.data in 0.088 s
```

We can use `SIAMCAT` for feature filtering as well:


```r
sc.obj <- filter.features(sc.obj, filter.method = 'abundance', cutoff = 1e-03)
## Features successfully filtered
sc.obj <- filter.features(sc.obj, filter.method = 'prevalence', cutoff = 0.05, feature.type = 'filtered')
## Features successfully filtered
sc.obj
## siamcat-class object
## label()                Label object:         61 CTR and 53 CRC samples
## filt_feat()            Filtered features:    839 features after abundance, prevalence filtering
## 
## contains phyloseq-class experiment-level object @phyloseq:
## phyloseq@otu_table()   OTU Table:            [ 14213 taxa and 114 samples ]
## phyloseq@sam_data()    Sample Data:          [ 114 samples by 16 sample variables ]
```

Now, we can test the filtered feature for differential abundance with `SIAMCAT`:


```r
sc.obj <- check.associations(sc.obj, detect.lim = 1e-05, 
                             fn.plot = './associations.pdf')
```
![](association_testing_files/figure-html/sc_assoc_testing_real-1.png)<!-- -->

The associations metrics computed by `SIAMCAT` are stored in the `SIAMCAT` 
object and can be extracted, if you want to plot other things:


```r
df.assoc <- associations(sc.obj)
df.assoc %>% 
  ggplot(aes(x=fc, y=-log10(p.adj))) + 
    geom_point() + 
    xlab('Fold change')
```

![](association_testing_files/figure-html/volc_2-1.png)<!-- -->

# Exercise: KEGG and toxin data

* Load the KEGG ko matrix that we explored yesterday. Test the KEGG kos
for differential abundance and visualize the results. Which display makes
most sense for the KEGG data?

* In the data folder, you will find a table called `putative_fadA_genes.tsv`,
which contains the relative abundances of the genes that your HMM identified
as putative _fadA_ genes in the IGC. Test the genes for differential abundance
and visualize as heatmap. What happens when you vary the E-value cutoff?

* (Optional) Using the KEGG module definitions that we explored yesterday, 
summarize each module and test for differential abundance. Which module is 
most strongly enriched in colorectal cancer? Create a heatmap of KEGG kos for 
this module.

* (Optional) Using the KEGG ko matrix, you can also check out the 
[FuncTree2](https://bioviz.tokyo/functree2/)
comparisons. Which measures would you like to visualize on the tree?




# (Optional) Visualization using `iTOL`

If we want to visualize the results of the differential abundance testing 
along the phylogenetic tree, we can use the [`iTOL`](https://itol.embl.de) tool
to display the phylogenetic tree and other measures of enrichment.

## Preparations

First, we have to load the phylogenetic tree for all mOTUs.
It can be found in the database that comes together with the
mOTUs profiler, and can also be found in the 
[Zenodo repository](https://zenodo.org/record/3366460).


```r
motus.tree <- read.tree('../../data/motus_tree/mOTUS_tree_2.5.nwx')
head(motus.tree$tip.label)
## [1] "ref_mOTU_v25_09471" "ref_mOTU_v25_09472" "ref_mOTU_v25_09892"
## [4] "ref_mOTU_v25_10779" "ref_mOTU_v25_06291" "ref_mOTU_v25_12077"
length(motus.tree$tip.label)
## [1] 14212
```

The names of the leaves in the tree are only the mOTU Identifiers (e.g. 
_ref_mOTU_v25_09912_). Therefore, we should force the names of the tip labels 
and our data to match up.

Additionally, the tree contains all possible mOTUs. Therefore, we will
have to prune the tree to contain only the mOTUs included in our analysis.

First, though, we will adjust the mOTU names in our results table a bit
for easier handling later one. As `ape` and `iTOL` perform some checks on the
names of the leaves in the tree, it makes sense to have the names already
without any special characters or whitespaces. Therefore, we create an 
additional column called `tree_name`, in which we save the name that will be
displayed in the tree later on.


```r
df.assoc$mOTU <- rownames(df.assoc)
df.assoc$mOTU_ID <- str_extract(df.assoc$mOTU, '(ref|meta)_mOTU_v25_[0-9]{5}')
df.assoc$tree_name <- str_remove(df.assoc$mOTU, 
                                 '\\[(ref|meta)_mOTU_v25_')
df.assoc$tree_name <- str_remove(df.assoc$tree_name, 
                                 '\\]$')
df.assoc$tree_name <- make.names(df.assoc$tree_name)
```

Now, we prune the tree to contain only mOTUs which were present in our 
analysis after filtering.


```r
motus.tree.pruned <- keep.tip(motus.tree, df.assoc$mOTU_ID)
```

Then, we can adjust the labels of the leave of the tree, so that they will 
match up with our data:


```r
motus.tree.pruned$tip.label <- df.assoc$tree_name[
  match(motus.tree.pruned$tip.label, df.assoc$mOTU_ID)]
```

Now, we can export everything and load it into `iTOL`:


```r
write_tsv(df.assoc, path = './tree_table.tsv')
write.tree(motus.tree.pruned, file = './motus_tree_pruned.nw')
```

## iTOL

These are some easy steps to get an annotated tree using the two files which
we generated above.

Go to [https://itol.embl.de](https://itol.embl.de)
![](./itol_screen_shots/itol_1.png)

Click on `Upload` and upload the file `motus_tree_pruned.nw`
![](./itol_screen_shots/itol_2.png)

The output should look something like this:
![](./itol_screen_shots/itol_3.png)
You can edit the appearance of the tree using the `Basic` menu on the top
right, e.g. the font size for the labels.

In order to add graphical annotations to the tree, click on the `Datasets`
tab on the top right and then on `Create a dataset`
![](./itol_screen_shots/itol_4.png)

`iTOL` will ask you if you have the `iTOL annotation editor`. As you most 
likely do not have it, click on `Create using the web interface`,
![](./itol_screen_shots/itol_5.png)
and then choose the type of datasets you want to add.
![](./itol_screen_shots/itol_6.png)

Then, you can edit the raw data for this dataset by clicking on 
`Edit using the web interface`
![](./itol_screen_shots/itol_7.png)

Add the data which we exported into the `tree_table.tsv` file into the web
interface. Here it is important that the names of the leaves and the names in 
your data match up
![](./itol_screen_shots/itol_8.png)

Now, you can update the view and style it according to your needs/wishes
![](./itol_screen_shots/itol_9.png)


## Exercise

* Add the _P_-value to the tree. Which type of plot would you use?

* Add another layer showing the Phylum for each mOTU. For this, you will need
the taxonomy table, which is part of the 
[mOTUs database](https://zenodo.org/record/3366460).
