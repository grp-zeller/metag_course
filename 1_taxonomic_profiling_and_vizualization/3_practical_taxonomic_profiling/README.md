# Practical: Demonstration of mOTUs for taxonomic profiling

Objectives of the practical:
- Learn how to create a taxonomic profile for a (quality filtered) metagenomic sample using mOTUs2;
- Familiarise with the parameters and the output of a taxonomic profiling tool;
- Learn how to merge many taxonomic profile files (all the profiles from your study) into one file.

Required tools:
- [mOTUs2](https://github.com/motu-tool/mOTUs_v2) (install with [conda](https://anaconda.org/bioconda/motus))

Required file:
- filtered fastq files created in the previous practical
- [https://www.embl.de/download/zeller/metaG_course/motus_profiles/tax_profiles.tar.gz](https://www.embl.de/download/zeller/metaG_course/motus_profiles/tax_profiles.tar.gz) (~1 Mb)

## Taxonomic profiling

The majority of microbiome studies rely on an accurate identification of the microbes and quantification their abundance in the sample under study, a process called taxonomic profiling.

![alt text](https://www.embl.de/download/zeller/metaG_course/pics/taxonomic_profiling.png)

We would like to save the profile in a file like:
```
Bacteroides_vulgatus    0.34
Prevotella_copri        0.16
Eubacterium_rectale     0.10
...
```

And, if we pull together many samples:
```
                        sample_1  sample_2
Bacteroides_vulgatus        0.34      0.01
Prevotella_copri            0.16      0.42
Eubacterium_rectale         0.10      0.00
```


We use [mOTUs2](https://github.com/motu-tool/mOTUs_v2) to create taxonomic profiles of metagenomic samples.

## Exercises

1. Use `motus` (manual: [link](https://github.com/motu-tool/mOTUs_v2#simple-examples)) to create a profile from the files created by trimmomatic.
   <details><summary>SOLUTION</summary>
   <p>

   You can download the 4 files that you created with `trimmomatic` in the previous practical with (in case you don't have them):
   ```
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/filtered_1P
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/filtered_1U
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/filtered_2P
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/filtered_2U
   
   ```
   
   You can profile the sample with:
   
   ```
   motus profile -f filtered_1P -r filtered_2P -s filtered_1U,filtered_2U -o test_sample.motus
   ```
   
   Which produces (`head test_sample.motus`):
   ```
   # git tag version 2.5.1 |  motus version 2.5.1 | map_tax 2.5.1 | gene database: nr2.5.1 | calc_mgc 2.5.1 -y insert.scaled_counts -l 75 | calc_motu 2.5.1 -k mOTU -C no_CAMI -g 3 | taxonomy: ref_mOTU_2.5.1 meta_mOTU_2.5.1
   # call: python motus profile -f filtered_1P -r filtered_2P -s filtered_1U,filtered_2U -o test_sample.motus
   #consensus_taxonomy	unnamed sample
   Leptospira alexanderi [ref_mOTU_v25_00001]	0.0000000000
   Leptospira weilii [ref_mOTU_v25_00002]	0.0000000000
   Chryseobacterium sp. [ref_mOTU_v25_00004]	0.0000000000
   Chryseobacterium gallinarum [ref_mOTU_v25_00005]	0.0000000000
   Chryseobacterium indologenes [ref_mOTU_v25_00006]	0.0000000000
   Chryseobacterium artocarpi/ureilyticum [ref_mOTU_v25_00007]	0.0000000000
   Chryseobacterium jejuense [ref_mOTU_v25_00008]	0.0000000000
   ```
   In the profile, `ref_mOTU` represent species with a reference genome sequence in NCBI. While `meta_mOTU` represent species that have not been sequenced yet (they do not have a representative genome in NCBI).
   the `-1` at the end of the file (check with `tail test_sample.motus`), represents species that we know to be present, but are not able to profile. The `-1` is useful to have a correct evaluation of the relative abundance of all species (more info [here](https://github.com/motu-tool/mOTUs_v2/wiki/Explain-the-resulting-profile#-1)).
   </p> 

2. How many species are detected? How many are reference species and how many are unknown species?
   <details><summary>SOLUTION</summary>
   <p>

   We can have a look at the identified species that are different from zero with `cat test_sample.motus | grep -v "0.0000000000"`:
   ```
   # git tag version 2.5.1 |  motus version 2.5.1 | map_tax 2.5.1 | gene database: nr2.5.1 | calc_mgc 2.5.1 -y insert.scaled_counts -l 75 | calc_motu 2.5.1 -k mOTU -C no_CAMI -g 3 | taxonomy: ref_mOTU_2.5.1 meta_mOTU_2.5.1
   # call: python /Users/milanese/Dropbox/PhD/mOTUsv2_tool/2.5.1/mOTUs_v2/motus profile -f filtered_1P -r filtered_2P -s filtered_1U,filtered_2U -o test_sample.motus -t 2
   #consensus_taxonomy	unnamed sample
   Proteobacteria sp. [ref_mOTU_v25_00095]	0.0001743716
   Eggerthella lenta [ref_mOTU_v25_00719]	0.0001584466
   Ruminococcus bromii [ref_mOTU_v25_00853]	0.0040007632
   ```
   
   With `cat test_sample.motus | grep -v "0.0000000000" | grep -v "#" | wc -l`, we can see that we have 139 detected species.
   
   To count the number of ref-mOTUs you can use:
   ```
   cat test_sample.motus | grep -v "0.0000000000" | grep -c "ref_mOTU_"
   ```
   and for meta-mOTUs:
   ```
   cat test_sample.motus | grep -v "0.0000000000" | grep -c "meta_mOTU_"
   ```
   </p> 


3. Can you change some parameters in `motus` to profile more or less species? 
   <details><summary>SOLUTION</summary>
   <p>

   In the mOTUs wiki page there are more information: [link](https://github.com/motu-tool/mOTUs_v2/wiki/Increase-precision-or-recall).  
   Basically, you can use `-g` and `-l` to increase(/decrease) the specificity of the detected species. Lower values allows you to profile more species (lax thresholds); while higher values corresponds to stringent cutoffs (and hence less profiled species).  

   If we run:
   ```
   motus profile -f filtered_1P -r filtered_2P -s filtered_1U,filtered_2U -o test_sample_relax.motus -t 2 -g 1 -l 40
   cat test_sample_relax.motus | grep -v "0.0000000000" | grep -v "#" | wc -l
   ```
   We obtain 218 species (79 more species than the default parameters). While, with:
    ```
   motus profile -f filtered_1P -r filtered_2P -s filtered_1U,filtered_2U -o test_sample_stringent.motus -t 2 -g 6 -l 90
   cat test_sample_stringent.motus | grep -v "0.0000000000" | grep -v "#" | wc -l
   ```
   we profile only 86 species (53 less than using the standard parameters).
   
   If you don't manage to run them (~4 mins each), you can download the profiles with:
   ```
   wget https://www.embl.de/download/zeller/metaG_course/motus_profiles/test_sample/test_sample.motus
   wget https://www.embl.de/download/zeller/metaG_course/motus_profiles/test_sample/test_sample_relax.motus
   wget https://www.embl.de/download/zeller/metaG_course/motus_profiles/test_sample/test_sample_stringent.motus
   ```
   </p> 


4. How can you merge different motus profiles into one file?
   Download the following profiles and merge them into one file.
   ```
   wget https://www.embl.de/download/zeller/metaG_course/motus_profiles/tax_profiles.tar.gz
   tar -zxvf tax_profiles.tar.gz 
   ```
   How the merged file differs from the example above?
   
   <details><summary>SOLUTION</summary>
   <p>

   You can use `motus merge` (more info [here](https://github.com/motu-tool/mOTUs_v2/wiki/Merge-profiles-into-a-table)).  
   Note that the name of the samples is speciefied by `-n` when using `motus profile`. 
   
   You can run:
   ```
   motus merge -d tax_profiles -o all_samples.motus
   ```
   which looks like (`head all_samples.motus`):
   ```
   # motus version 2.5.1 | merge 2.5.1 | info merged profiles: # git tag version 2.5.1 |  motus version 2.5.1 | map_tax 2.5.1 | gene database: nr2.5.1 | calc_mgc 2.5.1 -y insert.scaled_counts -l 75 | calc_motu 2.5.1 -k mOTU -C no_CAMI -g 3 | taxonomy: ref_mOTU_2.5.1 meta_mOTU_2.5.1 
   # call: python /Users/milanese/Dropbox/PhD/mOTUsv2_tool/2.5.1/mOTUs_v2/motus merge -d tax_profiles -o all_samples.motus
   #consensus_taxonomy	VF_264	VF_47	VF_77	VF_90
   Leptospira alexanderi [ref_mOTU_v25_00001]	0.0000000000	0.0000000000	0.0000000000	0.0000000000
   Leptospira weilii [ref_mOTU_v25_00002]	0.0000000000	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium sp. [ref_mOTU_v25_00004]	0.0000000000	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium gallinarum [ref_mOTU_v25_00005]	0.0000000000	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium indologenes [ref_mOTU_v25_00006]	0.0000000000	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium artocarpi/ureilyticum [ref_mOTU_v25_00007]	0.0000000000	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium jejuense [ref_mOTU_v25_00008]	0.0000000000	0.0000000000	0.0000000000	0.0000000000
   ```
   
   Note that that if you want to load this table into another tool (for example R), you would like to skip the first 2 rows.
   
   </p> 
   
5. (bonus) Profile the unfiltered fastq files from the test sample and compare them to the filtered one. 
   <details><summary>SOLUTION</summary>
   <p>

   We can profile the unfiltered reads with:
   ```
   motus profile -f raw_reads_1.fastq -r raw_reads_2.fastq -t 2 -n unfiltered -o unfiltered.motus
   ```
   
   Let's have a look at the species that are profiled by both:
   ```
   motus merge -i unfiltered.motus,test_sample.motus | grep -v "0.0000000000" | grep -v "#"
   ```
   Which results in:
   ```
   Proteobacteria sp. [ref_mOTU_v25_00095]           	0.0002007051	0.0001743716
   Eggerthella lenta [ref_mOTU_v25_00719]           	0.0001496362	0.0001584466
   Ruminococcus bromii [ref_mOTU_v25_00853]         	0.0037746076	0.0040007632
   Bacteroides rodentium/uniformis [ref_mOTU_v25_00855]	0.0212321086	0.0214878111
   Anaerostipes hadrus [ref_mOTU_v25_00857]         	0.0001321414	0.0001399217
   Roseburia faecis [ref_mOTU_v25_00859]            	0.0003954637	0.0004187479
   Roseburia inulinivorans [ref_mOTU_v25_00860]	        0.0001544263	0.0001639940
   ```

   The two profiles are really similar. mOTUs is filtering the reads internally based on how good they map to the marker gene sequences; hence trimming and filtering the reads before will not affect much the profiles. For other analysis (like building metagenome-assembled genomes) trimming the reads improve the result.
   </p> 
   