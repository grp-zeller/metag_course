# Taxonomic profiling and analyses

1. **lecture**: taxonomic profiling  
  
2. **practical**: quality control and read trimming  
  
3. **practical**: demonstration of mOTUs for taxonomic profiling   
  
4. **lecture**: visual exploration
  
5. **practical**: exploratory data analysis
  