# Practical: Quality control and read trimming

Objectives of the practical:
- Familiarise with fastq files, how to look at them and what they represent;
- Learn how to understand if the reads in your files are of good quality;
- Learn how to remove and trim low quality reads.

Required tools:
- [trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) (install with [conda](https://anaconda.org/bioconda/trimmomatic))
- [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) (install with [conda](https://anaconda.org/bioconda/fastqc))

Required file:
- [https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_1.fastq](https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_1.fastq) (~22 Mb)
- [https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_2.fastq](https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_2.fastq) (~22 Mb)

## Exercises

1. Open the terminal, create a directory in the `~/Desktop` and download the two required files with `wget`.
   <details><summary>SOLUTION</summary>
   <p>

   ```
   cd ~/Desktop
   mkdir practical_2
   cd practical_2
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_1.fastq
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_2.fastq
   ```
   </p> 

2. Look at the fastq files, how are they structured?
   <details><summary>SOLUTION</summary>
   <p>


   When we work with metagenomic data we usually have two fastq files produced by
   the Illumina sequencer:
   - a file containing the forward reads
   - a file containing the reverse reads
   
   Usually the prefix of the file name is the same, and we have `_1_` for the file
   with forward reads and `_2_` for the file with reverse reads, example:
   ```
   HYG3LBGXC_261_1_19s00-sample119s004346_1_sequence.txt.gz
   HYG3LBGXC_261_1_19s00-sample119s004346_2_sequence.txt.gz
   ```
   
   A fastq file contains 4 lines for each read, with the following information:
   
   | Line | Description |
   | ------ | ------ |
   | 1 | A line starting with `@` and the read id |
   | 2 | The DNA sequence | 
   | 3 | A line starting with `+` and sometimes the same information as in line 1 | 
   | 4 | A string of characters that represents the quality score (same number of characters as in line 2) | 
   
   We can have a look at the first read (4 lines) with `head -n 4 raw_reads_1.fastq`:
   ```
   @read1
   CTCTAGCAGATACTCTCCCTATATGAACTCATGGGGGCGGGGATGCCCGTCCTGTGTAACAATAAAAAATAACCTTGATGAGGGCGGATAGATCCTACCT
   +
   BBBFFFF<FBFFFFFFIIIIBFBFFBFIIIIFFFIIIIIFFBFFFFFB77BBFFBBBBBBBBBBFFFB7<0BBBB<BBBBFBBBFFF<<7BBBFFFBBBB

   ```

   Each character in the fourth line can be converted to a quality score ([Phred-33](https://support.illumina.com/help/BaseSpace_OLH_009008/Content/Source/Informatics/BS/QualityScoreEncoding_swBS.htm)) from 1 to 40:
   ```
        Character: !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHI
                   |         |         |         |         |
    Quality score: 0........10........20........30........40 
   ```
   
   And, for each quality score there is an associated probability for correctly calling a base:
   
   | Quality Score | Probability of incorrect base call | Base call accuracy |
   | ------ | ------ | ------ | 
   | 10 | 1 in 10 | 90% |
   | 20 | 1 in 100 | 99% |
   | 30 | 1 in 1000 | 99.9% |
   | 40 | 1 in 10,000 | 99.99% |
   
   </p> 


3. Use `fastqc` ([link](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)) to check the quality of the reads in the two files.
   <details><summary>SOLUTION</summary>
   <p>
 
   Run on the terminal:
   ```
   fastqc raw_reads_1.fastq
   fastqc raw_reads_2.fastq
   ```
   
   We can open the [html file for the reverse reads](https://www.embl.de/download/zeller/metaG_course/pics/raw_reads_2_fastqc.html). There a bunch of graphs, but probably the most interesting is:
   
   ![alt text](https://www.embl.de/download/zeller/metaG_course/pics/average_quality_reverse_read_raw.png)
   
   This view shows an overview of the range of quality values across all bases at each position in the FastQ file. 
   For each position a BoxWhisker type plot is drawn with:
   - median as a red line,
   - the inter-quartile range (25-75%) as a yellow box,
   - upper and lower whiskers represent the 10% and 90% points,
   - the blue line represents the mean quality.
   
   The y-axis on the graph shows the quality scores. The higher the score the better the base call. The background of the graph divides the y axis into very good quality calls (green), calls of reasonable quality (orange), and calls of poor quality (red). The quality of calls on most platforms will degrade as the run progresses, so it is common to see base calls falling into the orange area towards the end of a read.
   
   </p> 
   

4. Have a look at the first four reads, which reads would you remove?
   <details><summary>SOLUTION</summary>
   <p>

   If you have a look at the first four reads with `head -n 16 raw_reads_1.fastq` and `head -n 16 raw_reads_2.fastq`, you can see that:
   - `@read1` in the forward file looks fine, while in the reverse file only 22 nucleotides have been sequenced:  
      ```
      @read1
      GCTCGATGGAGAGGGAAGGTT
      +
      BBBFFFFBFFFFFBBFFFIFF
      ```
   - `@read54` in the forward file has only 4 sequenced nucleotides and all other are N's:  
      ```
      @read54
      NATTNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
      +
      #AAA################################################################################################
      ```  
   - `@read315` in the forward file has only 2 low quality nucleotides at the end:
      ```
      @read315
      TTGTAGTGCTTGTCGAATTCGACGTAGTATTTGCCTACGAGGTGGTCGCCCTTCATGCCCGACGTGGCGGGCGTTTCGCCGTTGCCGTAGAGTTTCCANN
      +
      BBBFFFFFFFFFFIIIIIIIIIIIFFIFIIIIIIIIIIIIIIFIIIIIIIIIIIFFFFFFFBBBBFFFFFFB<0<7<B<B<<BB<B<7<<0000B<<<##
      ```
   - `@read337` is of low quality in both files.
   
   We would like to remove `@read1` reverse and `@read54` forward because they are of low quality. On the other hand, `@read315` forward looks fine, except for the last 2 nucleotides.
   Hence, we would like to trim only the low quality nucleotides at the end (or beginning) of the read.  
   For `@read337`, we would like to remove both forward and reverse.

   </p> 

5. Use `trimmomatic` ([trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic)) to remove short reads (<40 bp), low quality reads and remove low quality bases at the end/beginning of the reads. What files are produced by trimmomatic?
   <details><summary>SOLUTION</summary>
   <p>

   Use the following command:
   ```bash
   trimmomatic PE raw_reads_1.fastq raw_reads_2.fastq LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:40 -baseout filtered
   ```

   Which produces 4 files:
   - `filtered_1P`, containing the forward reads that pass the filter and have a mate (in `filtered_2P`);
   - `filtered_1U`, containing the forward reads that pass the filter and do not have a mate (the paired reverse read didn't pass the filter)
   - `filtered_2P`, containing the reverse reads that pass the filter and have a mate (in `filtered_1P`);
   - `filtered_2U`, containing the reverse reads that pass the filter and do not have a mate (the paired forward read didn't pass the filter)
     
   </p> 

6. Check the filtered reverse reads with `fastqc`, is the read quality improved?
   <details><summary>SOLUTION</summary>
   <p>

   If we run again `fastQC` on the reverse paired end reads after filtering (`filtered_2P`), with:
   ```bash
   fastqc filtered_2P
   ```
   
   we obtain the following [html file](https://www.embl.de/download/zeller/metaG_course/pics/filtered_2P_fastqc.html). Now, we have better quality reads:
   
   ![alt text](https://www.embl.de/download/zeller/metaG_course/pics/compared_fastqc.png)
 

   </p> 



## References and additional information

1. [Intro to FastQC with explanation of a FASTA file](https://hbctraining.github.io/Intro-to-rnaseq-hpc-O2/lessons/02_assessing_quality.html)
2. [Per base sequence quality fastQC plot](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/Help/3%20Analysis%20Modules/2%20Per%20Base%20Sequence%20Quality.html)
3. [Trimmomatic manual](http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf)






