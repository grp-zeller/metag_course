---
title: "Exploratory Data Analysis for Taxonomic Profiles"
output:
  html_document:
    toc: yes
    toc_float: yes
    number_sections: yes
    keep_md: true
    df_print: paged
author: "Laura Carroll, Nicolai Karcher, Alessio Milanese, Jakob Wirbel and
  Georg Zeller"
---

In this practical, we are going to make ourselves familiar with taxonomic profiling data. 



# Preparation of the R environment

First, let's prepare our `R` environment:


```r
# load packages
library("tidyverse")
library("vegan")

# set a seed to make steps depending on random numbers reproducible
set.seed(2020)
```

# Loading the Data

Now, we can load the example dataset:


```r
tax.profiles <- read.table("../../data/motus_profiles/FR-CRC.motus", sep = '\t', quote = "",
                           comment.char = '', stringsAsFactors = FALSE, skip = 61, header = TRUE, row.names = 1,
                           check.names = FALSE)
tax.profiles <- as.matrix(tax.profiles)
print(dim(tax.profiles))
## [1] 14213   156
data.frame(tax.profiles[1:10, 1:5])
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":[""],"name":["_rn_"],"type":[""],"align":["left"]},{"label":["CCIS27304052ST.3.0"],"name":[1],"type":["int"],"align":["right"]},{"label":["CCIS13047523ST.4.0"],"name":[2],"type":["int"],"align":["right"]},{"label":["CCIS15794887ST.4.0"],"name":[3],"type":["int"],"align":["right"]},{"label":["CCIS94603952ST.4.0"],"name":[4],"type":["int"],"align":["right"]},{"label":["CCIS74726977ST.3.0"],"name":[5],"type":["int"],"align":["right"]}],"data":[{"1":"0","2":"0","3":"0","4":"0","5":"0","_rn_":"'Candidatus Kapabacteria' thiocyanatum [ref_mOTU_v25_10354]"},{"1":"446","2":"569","3":"383","4":"1590","5":"2207","_rn_":"-1"},{"1":"0","2":"1","3":"0","4":"0","5":"0","_rn_":"Abiotrophia defectiva [ref_mOTU_v25_04788]"},{"1":"0","2":"0","3":"0","4":"0","5":"0","_rn_":"Absiella dolichum [ref_mOTU_v25_03694]"},{"1":"0","2":"0","3":"0","4":"0","5":"0","_rn_":"Acaricomes phytoseiuli [ref_mOTU_v25_06702]"},{"1":"0","2":"0","3":"0","4":"0","5":"0","_rn_":"Acaryochloris marina [ref_mOTU_v25_10941]"},{"1":"0","2":"0","3":"0","4":"0","5":"0","_rn_":"Acetanaerobacterium elongatum [ref_mOTU_v25_10763]"},{"1":"0","2":"0","3":"0","4":"0","5":"0","_rn_":"Acetitomaculum ruminis [ref_mOTU_v25_06703]"},{"1":"0","2":"0","3":"0","4":"0","5":"0","_rn_":"Acetivibrio ethanolgignens [ref_mOTU_v25_10828]"},{"1":"0","2":"0","3":"0","4":"0","5":"0","_rn_":"Acetoanaerobium sticklandii/noterae [ref_mOTU_v25_04651]"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

# Differences in Library Size

Due to differences in experimental procedure and in bacterial density, samples will have different total number of reads ('Sequencing depth' or 'library size').
The raw count data obtained from this (not normalized for library size) is unsuitable for downstream analysis. 


```r
motu.counts <- colSums(tax.profiles)
hist(motu.counts, 50, xlab='mOTU counts', main='', col = 'slategrey')
```

![](exploratory_data_analysis_files/figure-html/read_depth-1.png)<!-- -->

```r
range(motu.counts)
## [1]   402 30604
```

There are several different approaches to account for the differences in 
library size. 

## Rarefaction

Rarefaction is the process of random subsampling without replacement, so that
all samples will have the same number of reads. You can think of this as 'simulating' even library sizes for all samples. 
Rarefaction works, but is controversial in the field since one is discarding data.

In R, we can use the `rrarefy` function from the `vegan` package for 
rarefaction of taxonomic profiles. 

Since this procedure is random, it is important to record which random seed 
was used (see above) in order to be reproducible.

When using rarefaction, one does not use the smallest library size since this would typically mean throwing away too much data.
Thus, an intermediate rarefaction depth has to be chosen and all samples below that depth have to be discarded.


```r
rar_depth <- 3000
tax.profiles.rar <- tax.profiles[, -which(colSums(tax.profiles) < rar_depth)]
tax.profiles.rar <- t(rrarefy(t(tax.profiles.rar), rar_depth))

range(colSums(tax.profiles.rar))
## [1] 3000 3000
```

## Relative abundance

Alternatively, raw read counts are transformed into relative abundances 
(dividing each sample by its total number of reads). 
This procedure is also called "Total Sum Scaling" or TSS.
In `R` it can be done like that:


```r
rel.tax.profiles <- apply(tax.profiles, 2, function(x) x / sum(x))
```

Let's work with Total Sum Scaled data from here on.

# Exercise: Mean abundance and prevalence

Plot the mean abundance as well as the prevalence (fraction of samples containing a given species) over the data. How many species are found in less than 1% of the samples? What do you think might be a reason to explain this phenomenon? 

# Filtering

In order to restrict analysis of the gut community to species that are likely
to persist and fulfill a role in this ecosystem (versus transient
food-associated microbes or species whose non-zero abundance is due to mapping
artifacts), or simply for the practical purpose of having to deal with fewer,
likely important species, we sometimes remove species with extremely low
abundance or low prevalence (or both).

This filtering is done in an ad hoc fashion, as we lack a clear criterion for what 'transient' or 'ecologically important' community members actually are. We usually enforce a threshold on the maximum abundance (in any sample) and sometimes on the prevalence, too.


```r
species.max.value <- apply(rel.tax.profiles, 1, max)
hist(species.max.value, 100,
     main = 'Histogram of max-abundance values',
     xlab = 'max rel. ab.',
     col='slategrey')
```

![](exploratory_data_analysis_files/figure-html/abundance_filtering-1.png)<!-- -->


```r
species.max.value <- apply(rel.tax.profiles, 1, max)
hist(log10(species.max.value), 100,
     main = 'Histogram of max-abundance values',
     xlab = 'log10(max rel. ab.)', col='slategrey')
```

![](exploratory_data_analysis_files/figure-html/abundance_filtering_2-1.png)<!-- -->



```r
f.idx <- which(species.max.value > 1e-04)
rel.tax.profiles.filt <- rel.tax.profiles[f.idx,]
```

Typically, metagenomes will contain reads that do not 'belong' to any known species. These reads are grouped in a separate row corresponding to the number/fraction of unmapped reads. The name of this row depends on the taxonomic profiler used. mOTUs uses '-1' to indicate the unmapped fraction, so let's remove this row!



```r
idx.um <- which(rownames(rel.tax.profiles.filt) == '-1')
dim(rel.tax.profiles.filt)
## [1] 1735  156
rel.tax.profiles.filt <- rel.tax.profiles.filt[-idx.um,]
dim(rel.tax.profiles.filt)
## [1] 1734  156
```

# Alpha diversity

Alpha diversity metrics describe the taxonomic diversity within samples. There are several alpha diversity measures. For example, we can ask how many different species we see within samples. This is called 'richness'. 


```r
richness <- apply(rel.tax.profiles.filt, 2, function(x) sum(x > 0))
hist(richness, 20, col='slategrey')
```

![](exploratory_data_analysis_files/figure-html/richness-1.png)<!-- -->

Alternatively, we can measure species richness (described above) and evenness together. Evenness describes how differently (or evenly) bacteria are distributed in samples: If all bacterial abundances are the same, evenness is maximal. 

A typically employed measure here is the Shannon-index, defined as the negative sum of relative abundances times their natural logarithm, combining richness and evenness into a compound metric.


```r
shannon_index <- apply(rel.tax.profiles + 1e-5, 2, function(x){
  #return(-sum(x * log(x)))
  return(-1 * sum(x* log(x, exp(1))))
})
```

# Exercise: Species abundance distributions

Plot the abundance distributions of _Eubacterium rectale_, _Prevotella copri_ and _Fusobacterium nucleatum_. Compare the means and shapes of these distributions.

# Ordination Methods

One popular way to visualize taxonomic profiling data are ordination methods
such as Principal Coordinate Analysis (PCoA), which give a coarse visual overview of differences in taxonomic composition between samples. 
In order to compute a PCoA, we need to first compute pairwise dissimilarities between samples. 

There are many disimilarity measures to choose from. Among the more popular ones are:

* the Bray-Curtis dissimilarity,
* the log-Euclidean distance,
* or the UniFrac distance.

The PCoA algorithm takes this information as input and attempts to displays true pairwise dissimilarities as faithfully as possible in two dimensions.


```r
library("vegan")
library("labdsv")

tax.profiles.filt <- tax.profiles[rownames(rel.tax.profiles.filt),]
bc.dist <- vegdist(t(tax.profiles.filt))
pcoa.results <- pco(bc.dist)
plot(pcoa.results$points[,1], pcoa.results$points[,2],
     xlab = '1st Coordinate', ylab = '2nd Coordinate')
```

![](exploratory_data_analysis_files/figure-html/bray_curtis-1.png)<!-- -->

# (Optional) Heteroscedasticity

Heteroscedasticity describes the phenomenon when variables with a higher mean also exhibit higher variance, i.e. the mean and the variance of a variable are correlated.
Taxonomic abundance data is heteroscedastic.



```r
mean <- rowMeans(rel.tax.profiles.filt)
var <- apply(rel.tax.profiles.filt, 1, var)

plot(mean, var, xlab = 'Mean abundance', ylab='Variance', log = 'xy')
```

![](exploratory_data_analysis_files/figure-html/heteroscadiscity_plot-1.png)<!-- -->

Some statistical methods (e.g. analysis of variance, ANOVA) assume
homoscedastic data and their application to metagenomic data can thus be
problematic.
